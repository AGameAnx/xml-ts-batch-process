import * as fs from 'fs';
import Instance from './Instance';
import {basePath, basePathOrig} from './Settings';

export const globalIgnoredPaths: string[] = [];

function parseDir<T extends Instance>(
	out: T[],
	type: { new(path: string, name: string, ext: string): T },
	basePath: string,
	dir: string = '',
	recursive: boolean = true,
	acceptableFileExtensions: string[] = ['xml']
): void {
	dir.replaceAll('\\', '/');
	if (dir.charAt(dir.length-1) != '/') {
		dir += '/';
	}
	if (!fs.existsSync(basePath + dir)) {
		return;
	}
	const filenames: string[] = fs.readdirSync(basePath + dir);
	for (const file of filenames) {
		const path: string = `${dir}${file}`;
		if (globalIgnoredPaths.includes(path)) {
			continue;
		}
		let exists: boolean = false;
		for (const instance of out) {
			if (instance.path == path) {
				exists = true;
				break;
			}
		}
		if (exists) {
			continue;
		}
		const fullPath: string = `${basePath}${path}`;
		const stats: fs.Stats = fs.statSync(fullPath);
		if (stats.isDirectory()) {
			if (recursive)
				parseDir(out, type, basePath, path, recursive, acceptableFileExtensions);
		} else if (stats.isFile()) {
			const extPos: number = file.lastIndexOf('.');
			if (acceptableFileExtensions.includes(file.substring(extPos + 1)))
				out.push(Instance.fromPath(type, path));
		}
	}
}

/**
 * @param type Type of Instance to create
 * @param dir 
 * @param recursive Specifies whether to search within the directory recursively
 * @param checkOrigDir 
 * @param acceptableFileExtensions list of acceptable file extensions
 * @returns An array of Instance type objects which correspond to xml files within a specified directory
 */
export function get<T extends Instance>(
	type: { new(path: string, name: string, ext: string): T },
	dir: string = '',
	recursive: boolean = true,
	origDir: boolean = true,
	acceptableFileExtensions: string[] = ['xml']
): T[] {
	const result: T[] = [];
	parseDir(result, type, basePath, dir, recursive, acceptableFileExtensions);
	if (origDir) {
		parseDir(result, type, basePathOrig, dir, recursive, acceptableFileExtensions);
	}
	return result;
}

/**
 * Load an array of instances
 */
export const load = (instances: Instance[]) => instances.forEach(instance => instance.load());

/**
 * Asynchronously load an array of instances
 */
export const loadAsync = (instances: Instance[]) => Promise.all(instances.map(instance => instance.load()));

/**
 * Load an array of instances
 */
export const unload = (instances: Instance[]) => instances.forEach(instance => instance.load());

/**
 * Universal synchronous process function
 */
export function process<T extends Instance>(
	type: { new(path: string, name: string, ext: string): T },
	fn: (instance: Instance) => void,
	dir: string = '',
	recursive: boolean = true,
	workOnOriginalInstances: boolean = true
): void {
	get(type, dir, recursive, workOnOriginalInstances).forEach(instance => {
		instance.load();
		fn(instance);
		instance.unload();
	});
}

export function processFileList<T extends Instance>(
	type: {
		new(path: string, name: string, ext: string): T,
		fromPath(type: { new(path: string, name: string, ext: string): T
	}, path: string) },
	paths: string[],
	fn: (instance: Instance) => void
): void {
	const instances: T[] = [];
	for (const path of paths) {
		instances.push(type.fromPath(type, path));
	}
	instances.forEach(instance => {
		instance.load();
		fn(instance);
		instance.unload();
	});
}

export async function processFileListAsync<T extends Instance>(
	type: {
		new(path: string, name: string, ext: string): T,
		fromPath(type: { new(path: string, name: string, ext: string): T
	}, path: string) },
	paths: string[],
	fn: (instance: Instance) => Promise<void>
): Promise<void[]> {
	const promises: Promise<void>[] = [];
	for (const path of paths) {
		const fnProcess = async () => {
			const instance = type.fromPath(type, path);
			await instance.promise_load();
			await fn(instance);
			instance.unload();
		};
		promises.push(fnProcess());
	}
	return Promise.all(promises);
}

/**
 * Universal process function with async load
 */
export async function processAsyncLoad<T extends Instance>(
	type: { new(path: string, name: string, ext: string): T },
	fn: (instance: Instance) => void,
	dir: string = '',
	recursive: boolean = true,
	workOnOriginalInstances: boolean = true
): Promise<void> {
	const instances = get(type, dir, recursive, workOnOriginalInstances);
	await loadAsync(instances);
	for (const instance of instances)
		fn(instance);
	unload(instances);
}

/**
 * Universal async process function
 */
export async function processAsyncOrdered<T extends Instance>(
	type: { new(path: string, name: string, ext: string): T },
	fn: (instance: Instance) => Promise<void>,
	dir: string = '',
	recursive: boolean = true,
	workOnOriginalInstances: boolean = true
): Promise<void> {
	const instances = get(type, dir, recursive, workOnOriginalInstances);
	await loadAsync(instances);
	for (const instance of instances)
		await fn(instance);
	unload(instances);
}

/**
 * Universal async process function, process all instances without concern for execution order
 */
export async function processAsyncOutOfOrder<T extends Instance>(
	type: { new(path: string, name: string, ext: string): T },
	fn: (instance: Instance) => Promise<void>,
	dir: string = '',
	recursive: boolean = true,
	workOnOriginalInstances: boolean = true
): Promise<void> {
	const instances = get(type, dir, recursive, workOnOriginalInstances);
	await loadAsync(instances);
	const promises: Promise<void>[] = [];
	for (const instance of instances)
		promises.push(fn(instance));
	await Promise.all(promises);
}
