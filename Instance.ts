/* eslint-disable @typescript-eslint/no-explicit-any */

import * as fs from 'fs';
import { XMLParser, XMLBuilder } from 'fast-xml-parser';
import { basePath, basePathOrig } from './Settings';

const xmlParser = new XMLParser({
	ignoreAttributes: false,
	preserveOrder: true,
	attributeNamePrefix : ''
});
const xmlBuilder = new XMLBuilder({
	ignoreAttributes: false,
	format: true,
	indentBy: '\t',
	suppressEmptyNode: true,
	attributeNamePrefix: '',
	attributesGroupName: ':@'
});

export default class Instance
{
	public readonly path: string;
	public readonly saveDir: string;
	public readonly filename: string;
	public readonly ext: string;
	public readonly name: string;
	public onlyBasePathOrig: boolean; 
	protected xmlInitial: string;
	public xml: any = {};

	public constructor(path: string, name: string, ext = 'xml', onlyBasePathOrig = false) {
		this.path = path;
		this.filename = name + '.' + ext;
		this.name = name;
		this.ext = ext;
		this.onlyBasePathOrig = onlyBasePathOrig;

		this.saveDir = basePath + path.substring(0, path.length - this.filename.length);
	}

	static fromPath<T extends Instance>(
		type: { new(path: string, name: string, ext: string, onlyBasePathOrig: boolean): T },
		path: string,
		onlyBasePathOrig: boolean = false
	): T {
		const filename = path.substring(path.lastIndexOf('/')+1);
		const extPos: number = filename.lastIndexOf('.');
		return new type(path, filename.substring(0, extPos), filename.substring(extPos + 1), onlyBasePathOrig);
	}

	protected getLoadPath(): string {
		return (!this.onlyBasePathOrig && fs.existsSync(basePath + this.path) ? basePath : basePathOrig) + this.path;
	}

	/**
	 * Parses xml from the original file
	 */
	public load(): void {
		this.xml = xmlParser.parse(fs.readFileSync(this.getLoadPath()));
		this.xmlInitial = JSON.stringify(this.xml);
	}

	/**
	 * Parses xml from the original file
	 */
	public async promise_load(): Promise<void> {
		this.xml = xmlParser.parse(await fs.promises.readFile(this.getLoadPath()));
		this.xmlInitial = JSON.stringify(this.xml);
	}

	public hasChanges(): boolean {
		return JSON.stringify(this.xml) != this.xmlInitial;
	}

	/**
	 * Resets xml variable
	 */
	public unload(): void {
		this.xml = {};
	}

	protected buildXML(): string {
		return xmlBuilder.build(this.xml);
	}

	public save(force: boolean = false): void {
		if (force || this.hasChanges()) {
			fs.mkdirSync(this.saveDir, { recursive: true });
			fs.writeFileSync(basePath + this.path, this.buildXML());
		}
	}

	public async promise_save(force: boolean = false): Promise<void> {
		if (force || this.hasChanges()) {
			await fs.promises.mkdir(this.saveDir, { recursive: true });
			return fs.promises.writeFile(basePath + this.path, this.buildXML());
		}
	}

	protected getSaveInfo(path: string, fullPath:boolean): {dir: string, filename: string} {
		let filename: string;
		let dir: string;
		if (fullPath) {
			filename = path.substring(path.lastIndexOf('/')+1);
			dir = basePath + path.substring(0, path.length - filename.length);
		} else {
			filename = path;
			dir = this.saveDir;
		}
		return {dir: dir, filename: filename};
	}

	public saveAs(path: string, fullPath: boolean = false): void {
		const saveInfo = this.getSaveInfo(path, fullPath);
		fs.mkdirSync(saveInfo.dir, { recursive: true });
		fs.writeFileSync(saveInfo.dir + saveInfo.filename, this.buildXML());
	}

	public async promise_saveAs(path: string, fullPath: boolean = false): Promise<void> {
		const saveInfo = this.getSaveInfo(path, fullPath);
		await fs.promises.mkdir(saveInfo.dir, { recursive: true });
		return fs.promises.writeFile(saveInfo.dir + saveInfo.filename, this.buildXML());
	}
}
