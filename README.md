# XML batch processing tool

Allows quickly running typescript scripts on xml files.

Can be used for any xml batch processing. Originally created for usage with Company of Heroes 2, Company of Heroes 3 and Age of Empires 4 essence editor tuning xml instance files. Tested to be used with VSCode.

## Usage instructions
- Basic usage examples can be found in `examples.ts`.
- Debugging in VSCode executes `examples.ts`.
- Files are saved in `instances` folder by default.
- It is also possible to work in-place in the `instances` folder.
- Instances directory names can be changed in `Settings.ts`.
- `print` functions automatically output text to `output.txt`.
- `print` functions calls use a file write stream - they flush the buffer every 50 calls, when `printFlush` is called manually, or on execution end.
- See `InstancesHelper.ts` for the batch processing functions.
- It's possible to run batch scripts asynchronously, but unless you're doing many files operations or other operations which can have potential wait times, it's generally not worth it.

## Game specific instructions
- Extract/copy the instance files to `instances_orig` directory.
- It's possible to not save the files if they weren't modified, simply pass `false` to save functions.
- Helper functions and example scripts should be made available at a later date.
