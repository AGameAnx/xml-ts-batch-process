/* eslint-disable @typescript-eslint/no-explicit-any */

import * as fs from 'fs';

let printConsoleLog: boolean = true;

let flushCounter = 0;
const autoFlush = 50;

let fileHandle:fs.WriteStream;

let printLevel = 0;

export function printSetDefaultConsoleLog(val: boolean): void {
	printConsoleLog = val;
}

export function printInit(path: string = './output.log'): void {
	fileHandle = fs.createWriteStream(path);
	fileHandle.cork();
}

export function plevel(title: string = '_', fn: () => void, console_log: boolean | undefined = false): void {
	print(title + ':', console_log);
	++printLevel;
	fn();
	--printLevel;
}

export function title(title: string = '_', fn: () => void, console_log: boolean | undefined = false): void {
	if (console_log === false || console_log === undefined && !printConsoleLog)
		console.log(title);
	print(title + ':', console_log);
	++printLevel;
	fn();
	--printLevel;
}

export function print(out: any = '', console_log: boolean | undefined = false): void {
	const tabulationStr = '  '.repeat(printLevel);
	fileHandle.write(tabulationStr + out.toString() + '\n');
	if (++flushCounter > autoFlush)
		printFlush();
	if (console_log === true || console_log === undefined && printConsoleLog)
		console.log(tabulationStr + out);
}

export function printFlush(): void {
	fileHandle.uncork();
	fileHandle.cork();
	flushCounter = 0;
}

process.on('exit', function() {
	if (fileHandle) {
		fileHandle.end();
	}
});
