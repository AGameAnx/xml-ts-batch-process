import Instance from './Instance';
import { print } from './Print';
import * as InstancesHelper from './InstancesHelper';

function eachInstance(instance: Instance): void {
	print(`Sync processed: ${instance.name}`);
	// Do xml manipulation here
	instance.save(false); // Save if needed
	//instance.save(); // Force save
}
async function eachInstanceAsync(instance: Instance): Promise<void> {
	print(`Async processed: ${instance.name}`);
	// Do xml manipulation here
	await instance.promise_save(false); // Save if needed
	//await instance.promise_save(); // Force save
}

// This function isn't really needed, but it helps with promise awaits
async function run(): Promise<void> {
	console.log('running...');

	//fs.rmSync(BASE_PATH, { recursive: true }); // Remove previous modified files

	const dir: string = 'sbps/races/american/';

	// Process functions automatically load files before calling the functors

	print('[process]', true);
	InstancesHelper.process(Instance, eachInstance, dir); // The simple process function should be used in most cases, only use others if you do a lot of file operations

	print('[processAsyncLoad]', true);
	await InstancesHelper.processAsyncLoad(Instance, eachInstance, dir);

	print('[processAsyncOrdered]', true);
	await InstancesHelper.processAsyncOrdered(Instance, eachInstanceAsync, dir);

	print('[processAsyncOutOfOrder]', true);
	await InstancesHelper.processAsyncOutOfOrder(Instance, eachInstanceAsync, dir);

	// When getting files manually, files need to be loaded manually
	print('[manual]', true);
	const instances: Instance[] = InstancesHelper.get(Instance, dir);
	await InstancesHelper.loadAsync(instances);
	instances.forEach(eachInstance);
	InstancesHelper.unload(instances); // Unload simply sets .xml to {} so that the rest of the data can be garbage collected

	// Here we create our own instance and force save it with some custom values
	const myInstance: Instance = Instance.fromPath(Instance, 'custom_instance.xml');
	myInstance.xml.test = {__attrib: 1, value: 'testValue'};
	myInstance.save();
}

run().then(() => {
	print('Done!', true);
});
