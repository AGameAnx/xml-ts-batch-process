
export let basePath = './instances/';
export let basePathOrig = './instances_orig/';

export function setBasePath(newVal: string): void {
	basePath = newVal;
}
export function setBasePathOrig(newVal: string): void {
	basePathOrig = newVal;
}
